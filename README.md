[TOC]


- - - -


# [0. Vocabulary](#markdown-header-0-vocabulary)

* The _Player_ refers to the customer playing the game
* A _Unit_ is the unit of measure for the _Game Scene_; `1 Unit = 32px` for a texture
* A _UPS_ is a *unit per second*; it can, for example, describes an _Entity_ speed
* An _Entity_ is an element of the world which can be interacted with (_Character_, foe, object)
* The _Character_ refers to the _Entity_ controlled by the _Player_ during the _Game Scene_
* A _Foe_ is a malicious _Entity_ that may be encountered by the _Player_ during the _Game Scene_
* A _Characteristic_ is a numeric value used for describing or calculating interactions between _Entities_; _Characteristics_ are not necessarily bound to solely _Entities_, but may also affect an _Item_ or an _Accessory_; _Characteristics_ include:
  * _HP_, or *health points*; when the _HP_ _Characteristic_ of an _Entity_ hits `0`, the _Entity_ death event is triggered (can lead to the _Entity_ disappearing, staying in a "dead" state, changing form, etc.)
  * _Strength_; may increase _HP_ loss dealt to an _Entity_
  * _Resistance_; may decrease _HP_ loss for an _Entity_
* The _Game Scene_ refers to the main scene, where the _Player_ controls the _Character_, i.e. the overworld, inside dungeons, etc.
* An _Item_ is a wearable item, obtained by the _Player_, allowing the _Character_ to perform specific actions or attacks
* An _Accessory_ is a passive item, obtained by the _Player_, affecting the _Characteristics_ or _Contextual Actions_ of the _Character_
* A _Tile_ is an element with a size of 1x1 _Units_; a _Tile_ cannot be an _Entity_
* A _Ground Tile_ is a tile with both `X` and `Z` angles between `-45` and `45` degrees which can be traversed or walked on by an _Entity_
* A _Wall Tile_ is a tile with both `X` and `Z` angles between `-45` and `45` degrees which **cannot** be traversed or walked on by an _Entity_
* An _Entity Center_ is a point usually positioned in the middle of a sprite; it may however depend on the sprite
* A _Collision Box_ is an arbitrary box how an _Entity_ or a _Tile_ can collide with another _Entity_ or _Tile_; it is either rectangular or square-shaped, and is usually the same `X` and `Y` size than the _Entity_ or _Tile_ texture and shares the same center with the _Entity Center_, though it can depend on the sprite, the _Entity_ or the _Tile_


- - - -


# [1. Previous Zelda elements](#markdown-header-1-previous-zelda-elements)

* 8 directions including diagonals `ALttP`
* Weapons (sword, shield, obtainable items and  accessories)
* Passive, buff equipment: "accessories" `ALttP`
* Dungeons with a reward in the form of an item or an accessory
* Open-world `ALttP`
* A teaching companion (the owl): possibly the narrator `LA`
* Secret collectibles (shells) `LA`
* Monsters with simple patterns
* Islands - the world is composed of islands, which can be visited via a boat `WW`
* Bombs
* Caves, sometimes including a great reward (new equipment, lifebar upgrade) `ALttP`
* Closed/hidden caves openable with a bomb
  * Indicated by an interactible / breakable item or a crackled wall
* Lens of Truth, shows through hidden elements, fake walls, etc. `OoT`


- - - -


# [2. Randomly thrown ideas](#markdown-header-2-randomly-thrown-ideas)

* There should be a boat. And this boat should allow the player to move from island to island pretty quickly
* Also, to make sure the game isn't as boring as Wind Waker, there should either be many things to see / interact with / fight on the sea, or very little travel time between islands, or a combination of both
* No shitty phase shift fuckaroo, so no "Dark World", "Seasons Scepter" shit or anything
* There should be a way to reverse controls (but not general UI (or maybe?)) for lefties
* Multiple game difficulties, but each comes with its rules; e.g. in "hard" mode, no teleportation system and/or limited ammunitions for bow, bombs, etc.
  * Or two settings: a difficulty level, and a "game type": "player" or "bystander", play with teleportation, no ammunition system, etc.; "adventurer", play with more constraints
* Menu UIs could be integrated directly in the gameplay: the settings menu is an item or a specific place at your starting point, or can be found by talking to a saving point / statue / whatever the fuck, etc.
* [2D shadows!](http://ncase.me/sight-and-light/)
* 2D effects, such as reflection and refraction, distortion effect on sword swing, etc.
* **!HARD!** heightmaps on all the sprites to allow for shadows ON sprites
* A waterthrower, or snow balls, to put out fire
* Having a companion would be nice. Maybe a Navi-like without the annoying "hey, listen!" and the insults
* Combination system: you must first get the combination blueprint or whatever, and then you can start combining items' use, e.g. combine the bombs and the arrows to create a bomb-launcher
  * Should be completely optional, but help reach secret places, kind of like a New Game+
* UI texts on ground, walls, etc.. No text box or whatever, texts are displayed directly in the 3D world. Could then make it fun to have the character be death and the main boss be something like a painter *`probably a sucky idea`*
  * Or, OR, there could be a narrator narrating the quest of the hero in the "player" game type, and giving clues when player is stuck *`probably an AWESOME idea`*
* There could be a love triangle quest, where the _Player_ has to choose between two girls or something and get a reward based on his choice (the other reward should still be obtainable somehow)
* 3D would be AWESOME *`AWESOME idea`*
* Pots and other pickup-able elements should be throwable and also placable (for example, when no direction is input)
* Weapons:
  * Sword (3): level 1, attack 1; level 2, attack 2 + slice through shields; level 3, attack 4 + kill unkillable enemies + slice through shields *`could be a javelin at one point, because javelins rock`*
  * Bow (2): level 1, you have to charge the bow to launch the arrow further (at maximum, the arrow never bends track); level 2, crossbow *`could be a javelin at one point, because javelins rock`*
  * Hookshot (2): level 1, bring light items / enemies to you; level 2, get pulled to heavy items / enemies
  * Feather Sword (2): level 1, spin attack also sends a shockwave around, and normal attacks cast small wind gusts; level 2, fireproof (see combinations) *`could be the final sword or an alternate sword, could even be the javelin; could be obtainable via collectible feathers`*
  * Bombs (2): level 1, put the bomb where the player is, and explodes after a second; level 2, push a direction to make the bomb roll
  * Roc's Feather (1): jump above pits
  * Claws (2): level 1, dig through muddy walls and ground (like a shovel); level 2, climb specifically marked walls (with dents)
  * Mace (1): break breakable walls and crush fences
  * Bottle (1): can store stuff, like water, lava or potion
  * Leaf (1): can push back enemies
  * Whip (1): can attack from far away, and help reach new places
  * Paintbrush (1): can change the color of color-coded puzzle elements, should be obtainable quickly for speedrunning
  * Muncher (1): can munch weak enemies, gaining back hearts or something
* Accessories:
  * Shield (3): level 1, can defend in one direction, but can get stolen by specific enemies; level 2, can defend in one direction and cannot be stolen or destroyed
  * Armor (3): level 1, defense 1; level 2, defense 2; level 3, defense 4
  * Flattener (2): level 1, flatten the character to slide through small wall gaps and stuff; level 2, slide through wall cracks (explodable walls)
  * Lens of Truth (1): shows hidden bombable walls, interactible entities, etc.
  * Money Pouch (2): level 1 *`default`*, can bear up to 999 coins; level 2, can bear up to 9999 coins
  * Quiver (3): level 1 *`default`*, can bear up to 20 arrows; level 2, can bear up to 50 arrows; level 3, can bear up to 99 arrows
  * Bomb Pouch (4): level 1 *`default`*, can bear up to 4 bombs; level 2, can bear up to 12 bombs; level 3, can bear up to 30 bombs; level 4, can bear up to 99 bombs
  * Heavy Botes (1): when worn, player is less pulled towards pits / holes and knockback is reduced; also allows to dive underwater
  * Fire Magic (1): can be combined with another item to include fire damage / burning
  * Ice Magic (1): can be combined with another item to include "freezing" status ailment
  * Rope (1): can be combined with other items to create new items (with bow and arrow to create paths, with claws to create a hookshot, etc.); could maybe be used as an item, being able to be tied to stuff and make enemies / bosses trip, create paths, etc.
  * Radar (1): plays various sounds revealing hidden elements in a room, could be fun to have it be combined with another item to create the lens of truth
  * Spring or Spring Boots (1): allows the player to jump a story up. If there's no upper story, deals damage to player upon hitting the ceiling
* Combinations:
  * Sword (any) + Fire Magic: burning sword
  * Sword (any) + Ice Magic: freezing sword
  * Bow (any) + Fire Magic: burning arrows
  * Bow (any) + Fire Magic: freezing arrows
  * Bow (any) + Bombs (any): explosive (AoE) arrows
  * Feather Sword (1) + Fire Magic: feather gets burnt and must be reforged (fireproof)
  * Feather Sword (2) + Fire Magic: burning feather sword (shockwave only)
  * Feather Sword (any) + Ice Magic: freezing feather sword (shockwave only)
  * Bow (any) + Hookshot (any): use hookshot to stretch the bow, and when release, throws the arrow (with an AoE around the arrow) backwards *`crazy`*
* Blueprints for combinations: blueprints must be found in order to combine two elements (to ensure a good obtention curve of items and equipments)
* A "leader" system in the AI system: when the leader is defeated, members of the group react differently (get furious, elect a new leader, disband, etc.)


- - - -


# [3. Gameplay](#markdown-header-3-gameplay)

## [3.1. _Game Scene_](#markdown-header-31-game-scene)

### [3.1.1. Controls](#markdown-header-311-controls)

The controls are divided into three categories: **movement**, **action** and **menu**. By default, the screen is separated as follows:

```
+----------------+
|       |  Menu  |
|       |--------|
|  Mvt  | A1 | A2|
|       |    |   |
+----------------+
```

With each box corresponding to the touch area of the control, `Mvt` being the **movement** category, `Menu` being **menu** and `A1` and `A2` the **action**s.


#### [3.1.1.1. Movement](#markdown-header-3111-movement)

The _Player_ can move its avatar — the _Character_ — via the **movement** control category. When first touching the **movement** area, a virtual stick appears at the landing position of the finger. While still being pressed, if the finger is moved, the virtual stick gets inclined accordingly.


### [3.1.2. Movements](#markdown-header-312-movements)

The _Character_ can move in eight directions, in a circular fashion (moving diagonally does not stack horizontal and vertical movements, but rather causes the _Character_ to move a certain time-affected radius from its previous position in the input angle), and is controller by the _Player_ via the **movement** control defined in [`3.1.1.1`](#markdown-header-3111-movement).

The _Character_ is considered **standing** on a _Ground Tile_ if his _Entity Center_ is directly in contact with the _Ground Tile_.

The _Character_ is considered **blocked** by a _Wall Tile_ if his _Collision Box_ is directly in contact with the _Wall Tile_ and if the _Character_'s _Entity Center_ `Y` coordinate is between `Ground Tile Y - 0.5 Units` included and  `Ground Tile Y + 0.5 Units` excluded. If, because of the collision calculation system, a _Character_ overlaps with a _Wall Tile_, he must be moved back to a position where it's entire _Collision Box_ is outside the _Wall Tile_ _Collision Box_ — but still in direct contact — alongside the movement segment of the previous movement.

The _Player_ may encounter pits (or holes). When the _Character_ stands on a pit, he moves automatically towards the center of the pit by a speed following the equation: `f(x) = 1 - x` _UPS_, with `x` being the distance between the _Character_'s _Entity Center_ and the _Ground Tile_'s _Entity Center_. When the segment between the _Ground Tile_ center and the _Character_'s _Entity Center_ is below 0.5 _Units_, the _Character_ is considered falling in the pit. There are two types of pits: "damaging pits" and "gateway pits". The type of a pit is determined by its texture — slightly lighter for a gateway pit — and a boolean flag.

**Note**: appart from being controlled by an IA instead of the _Player_'s actions, the same rules also apply to other _Entities_, including interactive objects (e.g. pots or bushes).


### [3.1.3. Foes](#markdown-header-313-foes)

The _Player_ may encounter _Foes_.

_Foes_ have specific _Characteristics_ for interacting with the player.

#### [3.1.3.1. _Foes_ _Characteristics_](#markdown-header-3131-foes-characteristics)

_Foes_ _Characteristics_ include:
* a health points (_HP_) value; when being hit, the _Foe_ loses a certain amount (defined by both the _Weapon_ or _Item_ used by the _Player_ and the _Foe_'s _Resistance_
* a _Resistance_ value; can influence the _HP_ lost when an attack is dealt to the _Foe_


### [3.1.4. Interactive objects](#markdown-header-314-interactive-objects)

The _Player_ may encounter interactive objects.


- - - -


# [4. Graphics](#markdown-header-4-graphics)

Graphics should resemble Link’s Awakening or Survival Kids design choices, with enhanced palettes.

Palettes should include three to four colors per element on a sprite: a normal color, a light and a dark one, and optionally a reflective light one.
> _When to use 3 colors, and when to use 4? And what about metallic-looking elements?_ **FIXED**


- - - -


# [5. Sound](#markdown-header-5-sound)
