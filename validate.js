'use strict';

var fs = require('fs');
var inflect = require('i')();

var filePath = process.argv[2] || __dirname + '/README.md';

var initialize = function() {
  inflect.inflections.singular('foes', 'foe');
};

var buildDictionary = function(data) {
  var dict = [];

  data.split('\n').forEach(function(line) {
    var matches = line.match(/\s*\*\s*(The|An|A)?\s*_(([ ]*[a-zA-Z0-9\-]+[ ]*)+)_/);
    if (!matches)
      return ;

    dict.push(inflect.singularize(matches[2].toLowerCase()));
  });

  return dict.sort();
};

var buildUsedWords = function(data) {
  var usedWords = [];

  data.split('\n').forEach(function(line) {
    var matches = line.match(/_(([ ]*[a-zA-Z0-9\-]+[ ]*)+)_/g);
    if (!matches)
      return ;

    usedWords = usedWords.concat(matches.map(function(match) { return inflect.singularize(match.replace(/_/g, '').toLowerCase()); }));
  });

  return usedWords.sort().filter(function(elem, index, array) {
    if (0 < index && usedWords[index - 1] === usedWords[index])
      return false;
    return true;
  });
};

var checkUsedWords = function(dict, usedWords) {
  console.log('Using dictionary: ' + dict.map(function(elem) { return '"' + elem + '"'; }).join(', '));

  var errorWords = [];
  usedWords.forEach(function(word) {
    if (-1 === dict.indexOf(word))
      errorWords.push(word);
  });

  if (errorWords.length)
    console.error(errorWords.length + ' undefined words found: ' + errorWords.map(function(elem) { return '"' + elem + '"'; }).join(', '));
  else
    console.log('No undefined words found');
};


(function(finalCallback) {
  initialize();

  return fs.readFile(filePath, function(err, data) {
    if (err)
      return finalCallback(err);

    data = data.toString('utf8');

    var dict = buildDictionary(data);
    var usedWords = buildUsedWords(data);

    checkUsedWords(dict, usedWords);

    return finalCallback();
  });
})(function(err) {
  if (err) {
    console.error(err.stack || err.message || err);
    return process.exit(1);
  }

  return process.exit(0);
});
